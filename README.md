# kafkaism

Basic Kafka demo python project that has two programs: producer and consumer. Producer program waits for 
STDIN line input and publishes each line to Kafka with topic 'on_line'. Consumer program reads
this topic and stores them to a PostgreSQL database.

## Setup

### Python and requirements

This project uses Python3.6. Setup virtualenv for it with the command 

`virtualenv -p /usr/bin/python3.6 venv`

Substitute `/usr/bin/python3.6` with the path to your python3.6 executable if needed.

Activate virtualenv: `source venv/bin/activate`

Install requirements with `pip install -r requirements.txt`.

Create `.env` file by copying `.env_example` file with: `cp .env_example .env`. Modify `.env` file as needed.

### Kafka

This project uses Kafka v2.2. If you want to use some other version, change the api_version parameter 
in `consumer/__main__.py` and `producer/__main__.py`.

Default URL for the Kafka service is `homework-kafka-joonas-a41a.aivencloud.com:27735`. If you want 
to use a different URL, supply it with environment variable `KAFKA_URL`.

If you are connecting to Kafka with no SSL (e.g. locally), then set environment variable `KAFKA_INSECURE=True`
to enable insecure connection.

If you are running connecting to Kafka with SSL then you need to move your SSL certificates like so:

```
.keys/kafka/ca.pem (CA certificate for verifying the server)
.keys/kafka/service.cert (access certificate)
.keys/kafka/service.key (access key)
```

### Database

This project uses PostgreSQL v11 as its database. For the project to run, set the next environment variables in
`.env`:

```
DB_DATABASE_NAME
DB_HOST
DB_PORT
DB_USERNAME
DB_PASSWORD
```

## Running

Run the producer with `python producer` and consumer with `python consumer`. Both are needed to
run at the same time to enable full data flow.