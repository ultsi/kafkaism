import sys
import os
import json
from kafka import KafkaProducer
from dotenv import load_dotenv
load_dotenv()

PRODUCER_NAME = os.getenv('PRODUCER_NAME', default='test_producer')

# Connect to Kafka:
print("Connecting to Kafka server...")
try:
    KAFKA_URL = os.getenv('KAFKA_URL', default='homework-kafka-joonas-a41a.aivencloud.com:27735')
    producer = False
    if(os.getenv('KAFKA_INSECURE')):
        producer = KafkaProducer(bootstrap_servers=KAFKA_URL,
                                api_version=(2,2))
    else:
        producer = KafkaProducer(bootstrap_servers=KAFKA_URL,
                                security_protocol='SSL',
                                ssl_check_hostname=True,
                                ssl_cafile='.keys/kafka/ca.pem',
                                ssl_certfile='.keys/kafka/service.cert',
                                ssl_keyfile='.keys/kafka/service.key',
                                api_version=(2,2))
    status = {'status': 'online', 'sender': PRODUCER_NAME}
    producer.send("producer_status", bytes(json.dumps(status), 'utf8'))
    producer.flush()
except:
    print("Connection failed!")
    sys.exit(1)
print("Connected!")

# Main loop:
# publish everything that's read from STDIN
def run():
    print("Type a message to publish it to Kafka! Hit enter to send.")
    for line in sys.stdin:
        msg = {'sender': PRODUCER_NAME, 'msg': line}
        producer.send("on_line", bytes(json.dumps(msg), 'utf8'))
        producer.flush()
        print("Message published!")

if __name__ == '__main__':
    run()
