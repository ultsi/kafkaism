import psycopg2 
import sys
import json
import os
from kafka import KafkaConsumer, TopicPartition
from dotenv import load_dotenv
load_dotenv()

# Connect to Kafka:
### TODO: How to catch connection failure? This doesn't seem to throw anything...
print("Connecting to Kafka server...")
consumer = False
KAFKA_URL = os.getenv('KAFKA_URL', default='homework-kafka-joonas-a41a.aivencloud.com:27735')
if(os.getenv('KAFKA_INSECURE')):
    consumer = KafkaConsumer(bootstrap_servers=KAFKA_URL,
                            api_version=(2,2))
else:
    consumer = KafkaConsumer(bootstrap_servers=KAFKA_URL,
                            security_protocol='SSL',
                            ssl_check_hostname=True,
                            ssl_cafile='.keys/kafka/ca.pem',
                            ssl_certfile='.keys/kafka/service.cert',
                            ssl_keyfile='.keys/kafka/service.key',
                            api_version=(2,2))
consumer.assign([TopicPartition("on_line", 0)])

print("Connected!")

# Connect to PostgreSQL:
DB_DATABASE_NAME = os.getenv('DB_DATABASE_NAME')
DB_USERNAME = os.getenv('DB_USERNAME')
DB_PASSWORD = os.getenv('DB_PASSWORD')
DB_HOST = os.getenv('DB_HOST')
DB_PORT = os.getenv('DB_PORT')

if(not DB_DATABASE_NAME):
    print("Environment variable DB_DATABASE_NAME missing! Exiting...")
    sys.exit(1)
elif(not DB_USERNAME):
    print("Environment variable DB_USERNAME missing! Exiting...")
    sys.exit(1)
elif(not DB_PASSWORD):
    print("Environment variable DB_PASSWORD missing! Exiting...")
    sys.exit(1)
elif(not DB_HOST):
    print("Environment variable DB_HOST missing! Exiting...")
    sys.exit(1)
elif(not DB_PORT):
    print("Environment variable DB_PORT missing! Exiting...")
    sys.exit(1)

print("Connecting to PostgreSQL...")
conn = False
cur = False
try:
    conn = psycopg2.connect(database=DB_DATABASE_NAME, user=DB_USERNAME, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT)
    cur = conn.cursor()
    cur.execute('CREATE TABLE IF NOT EXISTS stored_msgs (id serial PRIMARY KEY, sender text, msg text, date timestamp default now());')
    conn.commit()
except Exception as e:
    print("Connection failed! Error: ", e)
    sys.exit(1)

print("Connected!")


# Main loop:
# Listen to events from 'on_line' topic and store the messages to database
def run():
    print("Listening to Kafka events to store messages into Postgresql database...")
    for event in consumer:
        msg = json.loads(event.value)
        print(f"New message from {msg['sender']}: {msg['msg']}")
        try:
            cur.execute('INSERT INTO stored_msgs (sender, msg) VALUES (%s, %s);', (msg['sender'], msg['msg']))
            conn.commit()
            print("Stored message into database successfully")
        except Exception as e:
            print("Failed to store message into PostgreSQL!")
            print(e)

if __name__ == '__main__':
    run()
    